extends Node2D

onready var zellij:Node = get_node("/root/zellij")
onready var zellig_files:Node = get_node("/root/zellij_files")

signal node_changed
signal segment_changed

const circle_definition:int = 128
const button_size:int = 16
const segment_color_default:Color = Color(0.59,1,1,0.5)
const segment_color_selected:Color = Color(0,1,1,1)

export(float) var display_size:float = 	400
export(float) var tile_size:float = 	50
export(bool) var diagonals:bool = 		true
export(int) var subdivision:int = 		2
export(bool) var nodes:bool = 			true setget set_nodes
export(bool) var segments:bool = 		true setget set_segments
export(bool) var labels:bool = 			false setget set_labels
export(bool) var pick_segment:bool = 	false setget set_pick_segment
export(bool) var generate:bool = 		false setget set_generate

var vp_size:Vector2 = Vector2.ZERO
# warning-ignore:integer_division
var button_hsize:int = button_size / 2
var button_hsize2:int = button_hsize*button_hsize

var resource_name:String = ''
var resource_path:String = ''

class NodeSorter:
	static func horizontal(a, b):
		if a.x < b.x:
			return true
		return false
	static func vertical(a, b):
		if a.y < b.y:
			return true
		return false

# list of segments, each element is an array of 2 vector2
var struct_segments:Array = []
# list of position of intersections
var struct_nodes:Array = []
# id of the last picked segment
var segment_picked:int = -1
# list of shapes, each shape is dictionary : 
# - nodes: list of node ids
# - surfaces: list of list of nodes, include symetric shapes
# - symetry: int
var shapes:Array = []

func serialise() -> Dictionary:
	var data:Dictionary = {}
	data.name = 			resource_name
	data.display_size = 	display_size
	data.diagonals = 		diagonals
	data.subdivision = 		subdivision
	data.segments  = 		struct_segments.duplicate()
	data.nodes  = 			struct_nodes.duplicate()
	data.shapes = 			[]
	for shape in shapes:
		if shape.nodes.empty():
			continue
		var shape_data:Dictionary = {
			'nodes': shape.nodes.duplicate(),
			'symetry': 'NONE'
		}
		match shape.symetry:
			zellij.SYMETRY.AXIAL_X:
				shape_data.symetry = "AXIAL_X"
			zellij.SYMETRY.AXIAL_Y:
				shape_data.symetry = "AXIAL_Y"
			zellij.SYMETRY.AXIAL_XY:
				shape_data.symetry = "AXIAL_XY"
			zellij.SYMETRY.CENTRAL:
				shape_data.symetry = "CENTRAL"
		data.shapes.append(shape_data)
	return data

func set_nodes( b:bool ) -> void:
	nodes = b
	if is_inside_tree():
		$nodes.visible = nodes

func set_segments( b:bool ) -> void:
	segments = b
	if is_inside_tree():
		$segments.visible = segments

func set_labels( b:bool ) -> void:
	labels = b
	if is_inside_tree():
		$labels.visible = labels

func set_pick_segment( b:bool ) -> void:
	pick_segment = b
	if !pick_segment:
		segment_picked = -1
		for i in range(0,struct_segments.size()):
			reset_segment(i)

func set_generate(b:bool) -> void:
	generate = false
	if b:
		# purging nodes
		struct_nodes = []
		struct_segments = []
		shapes = []
		highlight_shape(-1)
		# construction shapes
		add_struct_square( 1, 			0, 				diagonals,	subdivision )
		add_struct_square( 0.5*sqrt(2), 	0, 			false,	0 )
		add_struct_square( 0.5*sqrt(2), 	TAU / 8, 	false,	0 )
		# generation of intersections
		generate_nodes()
		# tmp svg
		update_svg()
		# regeneration of lines and buttons
		redraw()

func add_segment( start:Vector2, end:Vector2 ) -> void:
	struct_segments.append({
		"index": struct_segments.size(),
		"pos0": start, 
		"pos1": end,
		"id0": -1, 
		"id1": -1
	})

func add_struct_square( size:float, rot:float, diagonal:bool, subdiv:int ) -> void:
	var extents:Vector2 = Vector2.ONE * size * 0.5
	var pt0:Vector2 = (extents * Vector2(-1,-1)).rotated( rot )
	var pt1:Vector2 = (extents * Vector2(1,-1)).rotated( rot )
	var pt2:Vector2 = (extents * Vector2(1,1)).rotated( rot )
	var pt3:Vector2 = (extents * Vector2(-1,1)).rotated( rot )
	add_segment( pt0, pt1 )
	add_segment( pt1, pt2 )
	add_segment( pt2, pt3 )
	add_segment( pt3, pt0 )
	if diagonal:
		add_segment( pt0, pt2 )
		add_segment( pt1, pt3 )
	if subdiv > 0:
# warning-ignore:narrowing_conversion
		var count:int = pow(2,subdiv)
		var hstep:Vector2 = ( pt1-pt0 ) / count
		var vstep:Vector2 = ( pt2-pt1 ) / count
		for i in range( 1, count ):
			# vertical lines
			add_segment( pt0+hstep*i, pt3+hstep*i )
			# horizontal lines
			add_segment( pt0+vstep*i, pt1+vstep*i )

func display_circle( diameter:float ) -> void:
	var line:Line2D
	if $circle.get_child_count() == 0:
		line = $segment_tmpl.duplicate()
		$circle.add_child( line )
	else:
		line = $circle.get_child(0)
	line.clear_points()
	for i in range( 0, circle_definition + 1 ):
		var a:float = i * TAU / circle_definition
		line.add_point( Vector2(cos(a),sin(a)) * diameter * display_size * 0.5 )
	line.visible = true

func add_node( pos:Vector2 ) -> int:
	# distance computation is required to avoid dupicates...
	for i in range( 0, struct_nodes.size() ):
		var sn:Vector2 = struct_nodes[i]
		if pos == sn:
			return i
		if pos.distance_squared_to(sn) < 1e-4:
			return i
	struct_nodes.append( pos )
	return struct_nodes.size() - 1

func sort_nodes() -> void:
	var rows:Dictionary = {}
	for si in struct_nodes:
		var y:int = int(floor(si.y*100))
		if not y in rows:
			rows[y] = []
		rows[y].append(si)
	struct_nodes = []
	var sorty:Array = rows.keys()
	sorty.sort()
	for y in sorty:
		rows[y].sort_custom( NodeSorter, "horizontal" )
		struct_nodes += rows[y]
	# relinking all nodes!!!
	for ss in struct_segments:
		ss.id0 = add_node( ss.pos0 )
		ss.id1 = add_node( ss.pos1 )
	
func generate_nodes() -> void:
	# regeneration of nodes (to ensure correct ids)
	struct_nodes = []
	for ss in struct_segments:
		ss.id0 = add_node( ss.pos0 )
		ss.id1 = add_node( ss.pos1 )
	var pow10:int = 10
	while( pow10 <= struct_segments.size() ):
		pow10 *= 10
	# segment/segment intersections
	var tested_pairs:Array = []
	for seg in struct_segments:
		for other in struct_segments:
			if other.index == seg.index:
				continue
			var a:int = seg.index
			var b:int = other.index
			if a < b:
				a = other.index
				b = seg.index
			var pair_index = a * pow10 + b
			if not pair_index in tested_pairs:
				tested_pairs.append( pair_index )
				var intersec = Geometry.segment_intersects_segment_2d( seg.pos0, seg.pos1, other.pos0, other.pos1 )
				if intersec != null:
# warning-ignore:return_value_discarded
					add_node( intersec )
	sort_nodes()

func display_segments() -> void:
	while $segments.get_child_count() < struct_nodes.size():
		$segments.add_child( $segment_tmpl.duplicate() )
	for i in $segments.get_child_count():
		var l:Line2D = $segments.get_child(i)
		if i >= struct_segments.size():
			l.visible = false
		else:
			var seg:Dictionary = struct_segments[i]
			l.clear_points()
			l.add_point( seg.pos0 * display_size )
			l.add_point( seg.pos1 * display_size )
			reset_segment( i )
			l.visible = true

func display_buttons() -> void:
	# displaying buttons
	while $nodes.get_child_count() < struct_nodes.size():
		var btn:TextureButton = $btn_tmpl.duplicate()
# warning-ignore:return_value_discarded
		btn.connect( "smart_button_pressed", self, "node_pressed" )
		$nodes.add_child( btn )
		var lbl:Label = btn.get_child(0)
		btn.remove_child( lbl )
		$labels.add_child( lbl )
	for i in $nodes.get_child_count():
		var btn:TextureButton = $nodes.get_child(i)
		if i >= struct_nodes.size():
			btn.name = ""
			btn.data = null
			btn.visible = false
			$labels.get_child(i).visible = false
		else:
			var snode:Vector2 = struct_nodes[i]
			btn.rect_position = snode * display_size - Vector2.ONE * button_hsize
			var bi_str:String = str(i)
			while bi_str.length() < 3:
				bi_str = '0'+bi_str
			btn.name = bi_str
			btn.data = { 
				'id': i, 
				'pos': snode
			}
			btn.visible = true
			var lbl:Label = $labels.get_child(i)
			lbl.text = bi_str
			lbl.visible = true
			lbl.rect_position = btn.rect_position + Vector2(button_hsize+2,-4)
	$nodes.visible = nodes

func display_shapes() -> void:
	while $shapes.get_child_count() < shapes.size():
		var shp:Node2D = $shape_tmpl.duplicate()
		$shapes.add_child( shp )
		shp.position = Vector2.ZERO
	# regen colors
	for i in range(0,$shapes.get_child_count()):
		if i >= shapes.size():
			clear_shape( i )
			continue
		display_shape( i )

func clear_shape( sid:int ) -> void:
	if sid < 0 or sid >= $shapes.get_child_count():
		return
	var shp:Node2D = $shapes.get_child(sid)
	for c in shp.get_children():
		if c is Line2D:
			c.clear_points()
		c.visible = false
	shp.visible = false

func display_shape( sid:int ) -> void:
	
	if sid < 0 or sid >= shapes.size() or sid >= $shapes.get_child_count():
		return
	
	var data:Dictionary = shapes[sid]
	var shp:Node2D = $shapes.get_child(sid)
	
	if data.nodes.empty():
		clear_shape( sid )
		return
	
	var border:Line2D = null
	for c in shp.get_children():
		if c is Line2D:
			border = c
	
	if border != null:
		border.clear_points()
		for nid in data.nodes:
			border.add_point( struct_nodes[nid] * display_size )
		if data.nodes.size() > 2:
			border.add_point( struct_nodes[data.nodes[0]] * display_size )
		border.visible = true
	
	shp.visible = true

func display_shape_seg( prev:Vector2, pt:Vector2, left:float, last:bool = false ) -> void:
	var diff:Vector2 = pt-prev
	var diffl:float = diff.length()
	if diffl > 25:
		var dir:Vector2 = diff / diffl
		var perp:Vector2 = Vector2( -dir.y, dir.x )
		var mid:Vector2 = diff/2
		$shape_border.add_point( prev + mid )
		$shape_border.add_point( prev + mid + perp * 5 * left )
		$shape_border.add_point( prev + mid + dir * 10 )
		if !last:
			$shape_border.add_point( pt )
	else:
		if !last:
			$shape_border.add_point( pt )
		elif diffl <= 25:
			$shape_border.add_point( prev + (pt-prev)*0.5 )
		else:
			$shape_border.add_point( prev + diff.normalized() * (diffl - 10) )

func display_shape_border( sid:int ) -> void:
	if sid < 0 or sid >= shapes.size():
		return
	$shape_border.clear_points()
	var node_list:Array = shapes[sid].nodes
	var count:int = node_list.size()
	var prev:Vector2 = Vector2.ZERO
	var left:float = 1
	# detect rotation
	if node_list.size() > 2:
		var first:Vector2 = (struct_nodes[node_list[1]]-struct_nodes[node_list[0]]).normalized()
		var second:Vector2 = (struct_nodes[node_list[2]]-struct_nodes[node_list[1]]).normalized()
		if second.angle_to( first ) < 0:
			left = -1
		
	for i in range(0,count):
		var pt:Vector2 = struct_nodes[node_list[i]] * display_size
		if i == 0:
			$shape_border.add_point( pt )
		else:
			display_shape_seg( prev, pt, left )
		prev = pt
		if i == count-1:
			pt = struct_nodes[node_list[0]] * display_size
			display_shape_seg( prev, pt, left, true )
	$shape_border.visible = true

func highlight_shape( sid:int ) -> void:
	if sid < 0 or sid >= shapes.size():
		$shape_border.visible = false
		reset_nodes()
		return
	var node_list:Array = shapes[sid].nodes
	for i in range(0,$nodes.get_child_count()):
		$nodes.get_child(i).pressed = i in node_list
	display_shape_border( sid )

func node_exists( p:Vector2 ) -> int:
	# distance computation is required to avoid dupicates...
	var closest:float = 0
	var i:int = -1
	for si in range(0,struct_nodes.size()):
		var d:float = p.distance_squared_to(struct_nodes[si])
		if d < 1e-2 and ( i == -1 or closest > d ):
			closest = d
			i = si
	if i != -1:
		return i
	return -1

func get_node_at( i:int ):
	if i < 0 or i >= struct_nodes.size():
		return null
	return struct_nodes[i]

func segment_exists( n0:int, n1:int ) -> int:
	if n0 < 0 or n0 >= struct_nodes.size() or n1 < 0 or n1 >= struct_nodes.size():
		return -1
	for i in range(0,struct_segments.size()):
		var ss:Dictionary = struct_segments[i]
		if ( ss.id0 == n0 and ss.id1 == n1 ) or ( ss.id0 == n1 and ss.id1 == n0 ):
			return i
	return -1

func get_segment_at( i:int ) -> Dictionary:
	if i < 0 or i >= struct_segments.size():
		return {}
	return struct_segments[i]

func node_pressed( btn:TextureButton ) -> void:
	emit_signal( "node_changed", btn )

func reset_nodes() -> void:
	for c in $nodes.get_children():
		c.pressed = false

func create_segments( pairs:Array ) -> void:
	var do_refresh:bool = false
	for pair in pairs:
		if create_segment( pair[0], pair[1], false ):
			do_refresh = true
	if do_refresh:
		# generation of intersections
		generate_nodes()
		# regeneration of lines and buttons
		redraw()

func create_segment( n0:int, n1:int, refresh:bool =  true ) -> bool:
	if n0 == n1 or n0 == -1 or n1 == -1 or n0 >= struct_nodes.size() or n1 >= struct_nodes.size():
		return false
	# quick search in segment to be certain it's not already created
	for ss in struct_segments:
		if ( ss.id0 == n0 and ss.id1 == n1 ):
			return false
		if ( ss.id0 == n1 and ss.id1 == n0 ):
			return false
	# slower search to avoid overlapping segments
#	for ss in struct_segments:
#		var cpt0:Vector2 = Geometry.get_closest_point_to_segment_2d( n0.data.pos, ss.pos0, ss.pos1 )
#		var cpt1:Vector2 = Geometry.get_closest_point_to_segment_2d( n1.data.pos, ss.pos0, ss.pos1 )
#		if cpt0.distance_to( n0.data.pos ) < 1e-2 and cpt1.distance_to( n1.data.pos ) < 1e-2:
#			return false
	add_segment( struct_nodes[n0], struct_nodes[n1] )
	if refresh:
		# generation of intersections
		generate_nodes()
		# regeneration of lines and buttons
		redraw()
	return true

func remove_segments( list:Array ) -> void:
	var zombies:Array = []
	for zi in list:
		if zi < 0 or zi >= struct_segments.size():
			continue
		zombies.append( struct_segments[zi] )
	if zombies.empty():
		return
	for z in zombies:
		struct_segments.erase( z )
	# reindex all segments
	for i in range(0,struct_segments.size()):
		struct_segments[i].index = i
	# generation of intersections
	generate_nodes()
	# regeneration of lines and buttons
	redraw()
	segment_picked = -1

func reset_segment( i:int ) -> void:
	if i < 0 or i >= struct_segments.size():
		return
	$segments.get_child(i).width = 1
	$segments.get_child(i).default_color = segment_color_default

func highlight_segment( i:int ) -> void:
	if i < 0 or i >= struct_segments.size():
		return
	$segments.get_child(i).width = 2
	$segments.get_child(i).default_color = segment_color_selected

func get_shapes() -> Array:
	return shapes

func update_svg() -> void:
	var shape_list:Array = []
	for sha in shapes:
		for surf in sha.surfaces:
			if surf.empty():
				continue
			shape_list.append( surf )
	var res:Dictionary
	# edition svg
	res = zellig_files.generate_svg( shape_list, zellij.tmp_svg_edit, display_size )
	if res.success:
		$shape_svg.position = res.min
		$shape_svg.texture = zellig_files.get_external_texture( zellij.tmp_svg_edit )
	else:
		$shape_svg.texture = null
	# tiling svg
	res = zellig_files.generate_svg( shape_list, zellij.tmp_svg_tile, tile_size )

func add_shape() -> Array:
	shapes.append({
		'nodes': [],
		'surfaces': [[]],
		'symetry': zellij.SYMETRY.NONE,
	})
	display_shapes()
	return get_shapes()

func remove_shape( sid:int ) -> void:
	if sid < 0 or sid >= shapes.size():
		return
	shapes.remove( sid )
	update_svg()
	display_shapes()

func regenerate_surfaces( sid:int ) -> void:
	
	if sid < 0 or sid >= shapes.size():
		return
	
	var shape:Dictionary = shapes[sid]
	
	# regenerate surfaces
	match shape.symetry:
		zellij.SYMETRY.NONE:
			shape.surfaces = [[]]
		zellij.SYMETRY.AXIAL_X:
			shape.surfaces = [[],[]]
		zellij.SYMETRY.AXIAL_Y:
			shape.surfaces = [[],[]]
		zellij.SYMETRY.AXIAL_XY:
			shape.surfaces = [[],[],[],[]]
		zellij.SYMETRY.CENTRAL:
			shape.surfaces = [[],[]]
	
	var oid:int
	for nid in shape.nodes:
		var p:Vector2 = get_node_at( nid )
		shape.surfaces[0].append( p )
		match shape.symetry:
			zellij.SYMETRY.AXIAL_X:
				p.x *= -1
				if node_exists( p ) != -1:
					shape.surfaces[1].append( p )
			zellij.SYMETRY.AXIAL_Y:
				p.y *= -1
				if node_exists( p ) != -1:
					shape.surfaces[1].append( p )
			zellij.SYMETRY.AXIAL_XY:
				p.x *= -1
				if node_exists( p ) != -1:
					shape.surfaces[1].append( p )
				p.y *= -1
				if node_exists( p ) != -1:
					shape.surfaces[2].append( p )
				p.x *= -1
				if node_exists( p ) != -1:
					shape.surfaces[3].append( p )
			zellij.SYMETRY.CENTRAL:
				p *= -1
				if node_exists( p ) != -1:
					shape.surfaces[1].append( p )

func add_shape_node( sid:int, n:int ) -> void:
	if sid < 0 or sid >= shapes.size():
		return
	if not shapes[sid].nodes.empty():
		# back check
		for i in range(1,3):
			var ii:int = shapes[sid].nodes.size() - i
			if ii < 0:
				break
			if shapes[sid].nodes[ii] == n:
				return
		# front check
		if shapes[sid].nodes[0] == n:
			return
	shapes[sid].nodes.append( n )
	regenerate_surfaces( sid )
	update_svg()
	display_shape( sid )

func remove_shape_node( sid:int, n:int ) -> void:
	if sid < 0 or sid >= shapes.size():
		return
	var p:int = shapes[sid].nodes.find_last(n)
	shapes[sid].nodes.remove(p)
	# check that first and last are not the same
	if shapes[sid].nodes.size() > 1:
		var lasti:int = shapes[sid].nodes.size()-1
		if shapes[sid].nodes[0] == shapes[sid].nodes[lasti]:
			shapes[sid].nodes.remove(lasti)
	regenerate_surfaces( sid )
	update_svg()
	display_shape( sid )

func set_shape_symetry( sid:int, sym:int ) -> void:
	if sid < 0 or sid >= shapes.size():
		return
	if shapes[sid].symetry == sym:
		return
	shapes[sid].symetry = sym
	regenerate_surfaces( sid )
	update_svg()
	display_shape( sid )

func get_shape( i:int ) -> Dictionary:
	if i < 0 or i >= shapes.size():
		return {}
	return shapes[i]

func vp_resized() -> void:
	vp_size = get_viewport().size
	position = vp_size * 0.5

func redraw() -> void:
	vp_resized()
	# regeneration of lines and buttons
	display_segments()
	display_buttons()
	display_circle( 1 )
	display_shapes()

func _ready():
	set_generate(true)
# warning-ignore:return_value_discarded
	get_viewport().connect("size_changed",self,"vp_resized")
	$nodes.visible = nodes
	$segments.visible = segments
	$labels.visible = labels

# warning-ignore:unused_argument
func _process(delta) -> void:
	
	if pick_segment:
		
		var mp:Vector2 = get_viewport().get_mouse_position()
		mp -= position
		mp /= display_size
		var abs_min:float = button_hsize2 / display_size
		var ls:int = segment_picked
		var closest:float = -1
		segment_picked = -1
		for i in range(0,struct_segments.size()):
			var seg:Dictionary = struct_segments[i]
			var cp:Vector2 = Geometry.get_closest_point_to_segment_2d( mp, seg.pos0, seg.pos1 )
			var d:float = cp.distance_squared_to( mp )
			if d < abs_min and ( closest == -1 or closest > d ):
				closest = d
				segment_picked = i
		if ls == segment_picked:
			# nothing to do
			pass
		else:
			for i in range(0,struct_segments.size()):
				reset_segment( i )
			if segment_picked != -1:
				highlight_segment( segment_picked )
			emit_signal( "segment_changed" )

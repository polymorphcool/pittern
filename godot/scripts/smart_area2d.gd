extends Area2D

signal smart_area_entered
signal smart_body_entered

var data = null

func load_polygon( positions:Array ) -> void:
	$collision.polygon = positions

func line(b:bool) -> void:
	if !b or $collision.polygon.size() == 0:
		$line.points = []
	else:
		var src:Array = $collision.polygon
		src.append( src[0] )
		$line.points = src

func surface(b:bool) -> void:
	if !b:
		$surface.polygon = []
	else:
		$surface.polygon = $collision.polygon

func color( c:Color ) -> void:
	$line.default_color = c
	$surface.color = c

func width( f:float ) -> void:
	$line.width = f

func _on_area_entered(area: Area2D) -> void:
	emit_signal( "smart_area_entered", self, area )

func _on_body_entered(body: Node) -> void:
	emit_signal( "smart_body_entered", self, body )

func _ready():
	connect( "area_entered", self, "_on_area_entered" )
	connect( "body_entered", self, "_on_body_entered" )

extends Node

func generate_content( lace ):
	
	var bbox = lace.bbox
	var stroke = lace.width
	
	var joint = "miter"
	if lace.joint_mode == Line2D.LINE_JOINT_BEVEL:
		joint = "bevel"
	elif lace.joint_mode == Line2D.LINE_JOINT_ROUND:
		joint = "round"

	var cap = "butt"
	if lace.begin_cap_mode == Line2D.LINE_CAP_ROUND:
		cap = "round"
	elif lace.begin_cap_mode == Line2D.LINE_CAP_BOX:
		cap = "square"
	
	var content = ""
	content += '<?xml version="1.0" encoding="UTF-8" standalone="no"?>\n'
	content += '<!-- Created with Laces (http://gitlab.com/polymorphcool/laces) -->\n'
	content += '<svg\n'
	content += '\txmlns:dc="http://purl.org/dc/elements/1.1/"\n'
	content += '\txmlns:cc="http://creativecommons.org/ns#"\n'
	content += '\txmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"\n'
	content += '\txmlns:svg="http://www.w3.org/2000/svg"\n'
	content += '\txmlns="http://www.w3.org/2000/svg"\n'
	content += '\txmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"\n'
	content += '\txmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"\n'
	content += '\twidth="' + str(bbox.size.x+stroke) + '"\n'
	content += '\theight="' + str(bbox.size.y+stroke) + '"\n'
	content += '\tviewBox="0 0 ' + str((bbox.size.x+stroke))
	content += ' ' + str((bbox.size.y+stroke)) + '"\n'
	content += '\tversion="1.1">\n'
	content += '\t<g\n'
	content += '\t\tinkscape:label="lace"\n'
	content += '\t\tinkscape:groupmode="layer">\n'
	content += '\t\t<path\n'
	content += '\t\t\tstyle="fill:none;stroke:#000000;'
	content += 'stroke-width:' + str(stroke) + ';'
	content += 'stroke-linecap:'+cap+';'
	content += 'stroke-linejoin:'+joint+';'
	content += 'stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"\n'
	
	var path = ""
	var pos = null
	
	var offset = Vector2( 
		-bbox.min.x + stroke * 0.5, 
		-bbox.min.y + stroke * 0.5 )
	
	for pt in lace.points:
		if pos == null:
			path += "M "
			# initial offset
			path += str( offset.x ) + "," + str( offset.y )
			pos = offset + pt
			path += ' ' + str( pos.x ) + "," + str( pos.y )
		else:
			pos = offset + pt
			path += " H " + str( pos.x )
			path += " V " + str( pos.y )
	
	content += '\t\t\td="' + path + '"\n'
	content += '\t\t\tid="path1"\n'
	content += '\t\t\tinkscape:connector-curvature="0" />\n'
	content += '\t</g>\n'
	content += '</svg>\n'
	
	return content

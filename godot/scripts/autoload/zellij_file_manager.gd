extends Node

onready var zellij:Node = get_node("/root/zellij")

func get_valid_path( name:String ) -> String:
	return zellij.default_save_folder + name + ".json"
	
func serialise_unit( data:Dictionary, path:String ) -> void:
	var json:File = File.new()
	if json.open( path, File.WRITE ) == OK:
		json.store_string( JSON.print(data,'\t',true) )
		json.close()

func get_external_texture(path):
	var img = Image.new()
	img.load( path )
	var texture = ImageTexture.new()
	texture.create_from_image( img )
	return texture

func generate_svg( shape_list:Array, path:String, svg_scale:float ) -> Dictionary:
	
	var res:Dictionary = {
		'success' : false,
		'min' : Vector2.ZERO,
		'max' : Vector2.ZERO,
		'size' : Vector2.ZERO,
	}
	
	var first:bool = true
	for pts in shape_list:
		for p in range(0,pts.size()):
			if first:
				first = false
				res.min = pts[p]
				res.max = pts[p]
			else:
				for i in range(0,2):
					if res.min[i] > pts[p][i]:
						res.min[i] = pts[p][i]
					if res.max[i] < pts[p][i]:
						res.max[i] = pts[p][i]
	res.min *= svg_scale
	res.max *= svg_scale
	res.size = res.max - res.min
	var content = ""
	content += '<?xml version="1.0" encoding="UTF-8" standalone="no"?>\n'
	content += '<!-- Created with Zellij (http://gitlab.com/polymorphcool/zellij) -->\n'
	content += '<svg\n'
	content += '\txmlns:dc="http://purl.org/dc/elements/1.1/"\n'
	content += '\txmlns:cc="http://creativecommons.org/ns#"\n'
	content += '\txmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"\n'
	content += '\txmlns:svg="http://www.w3.org/2000/svg"\n'
	content += '\txmlns="http://www.w3.org/2000/svg"\n'
	content += '\txmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"\n'
	content += '\txmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"\n'
	content += '\twidth="' + str(res.size.x) + '"\n'
	content += '\theight="' + str(res.size.y) + '"\n'
	content += '\tviewBox="0 0 ' + str(res.size.x) + ' ' + str(res.size.y) + '"\n'
	content += '\tversion="1.1">\n'
	content += '\t<g\n'
	content += '\t\tinkscape:label="zellij"\n'
	content += '\t\tinkscape:groupmode="layer">\n'
	# reframing points
	var sid:int = 0
	for pts in shape_list:
		content += '\t\t' + '<path\n'
		content += '\t\t\t' + 'style="fill:#' +  zellij.svg_tmp_fill.to_html(false) + ';'
		content += 'fill-opacity:' + str(zellij.svg_tmp_fill.a) + ';'
		content += ';stroke:none"\n'
		content += '\t\t\t' + 'd="M '
		for p in range(0,pts.size()):
			content += str(pts[p][0]*svg_scale-res.min[0]) + ',' + str(pts[p][1]*svg_scale-res.min[1]) + ' '
		content += 'Z"'
		content += '\t\t\t' + 'id="path' + str(sid) + '"/>'
		sid += 1
	content += '\t</g>\n'
	content += '</svg>\n'
	
	var f:File = File.new()
	if f.open( path, File.WRITE ) == OK:
		f.store_string( content )
		f.close()
		res.success = true
	
	return res

func _ready():
	var d:Directory = Directory.new()
	if not d.dir_exists( zellij.tmp_folder ):
		d.make_dir_recursive( zellij.tmp_folder )
	if not d.dir_exists( zellij.default_save_folder ):
		d.make_dir_recursive( zellij.default_save_folder )

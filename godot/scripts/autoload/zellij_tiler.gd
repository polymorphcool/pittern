extends Node

onready var zellij:Node = get_node("/root/zellij")

# SHAPE MANAGEMENT

signal shape_created
signal shape_validated
signal tiling_step
signal tiling_finished

var shape_area2d = preload("res://objects/shape_area2d.tscn").instance()

const display_collider_line:bool = true
const display_collider_surface:bool = false
const shape_collider_scale:float = 100.0
const shape_main_contraction:float = 0.1

onready var creation_thread:Thread = null
onready var creation_mutex:Mutex = Mutex.new()
onready var validation_mutex:Mutex = Mutex.new()

var shape_creation_requests:Array = []
var shape_validation_requests:Array = []
var tiling_requests:Array = []
# shape validation and tiling must be done iteratively
var current_shape_validation:Dictionary = {}
var current_tiling_request:Dictionary = {}

### CREATION / DATA STRUCTURES

# new shape structure
func new_shape() -> Dictionary:
	return {
		'colliders': [], # list of vertices groups
		'paths': [], # list of vertices groups
		'edges': [],
		'symmetry_axis': [],
		'symmetric': null,
		'clones': [],
		'cloneUID': 0,
		'min': null,
		'max': null,
		'size': null,
		'valid': false,
		'sorting': {
			'edge_small': [],
			'edge_big': [],
			'clone_rotation': [], # array of arrays! one for each edge
			'clone_alignment': [], # array of arrays! one for each edge
			'clone_perpendicular': [] # array of arrays! one for each edge
		}
	}

# new vertex object
func new_vertex( pos:Vector2, norm:Vector2 ) -> Dictionary:
	var v:Dictionary = {
		'pos': pos,
		'normal': norm
	}
	return v

# new vertex edge out of two vertices
func new_edge( uid:int, pid:int, v0:Dictionary, v1:Dictionary ) -> Dictionary:
	var e:Dictionary = {
		'uid': uid,
		'pid': pid, # path ID
		'v0': v0,
		'v1': v1,
		'dir': null,
		'normal': null,
		'length': 0,
		'next': null,
		'previous': null
	}
	e.dir = (v1.pos-v0.pos)
	e.mid = v0.pos + e.dir * 0.5
	e.length = e.dir.length()
	e.dir /= e.length
	e.normal = Vector2( e.dir.y, -e.dir.x )
	return e

# new shape clone, 
func new_clone() -> Dictionary:
	return {
		'uid': -1,
		'colliders': [],
		'paths': [],
		'symmetry': false,
		'offset': null,
		'rotation': 0,
		'alignment': -1,
		'valid':false
	}

# new tiling, with vertices and all required info
func new_tiling( shape:Dictionary, depth:int = 1, edge_sort:int = zellij.EDGE_SORT.DEFAULT, clone_sort:int = zellij.CLONE_SORT.DEFAULT ) -> Dictionary:
	return {
		'shape': shape,
		'levels': [],
		'depth_request': depth,
		'edge_sort': edge_sort,
		'clone_sort': clone_sort,
		'min': Vector2( shape.min ),
		'max': Vector2( shape.max ),
		'size': Vector2( shape.size ),
		'stats': {},
		'valid': false
	}

### UTILS

class TilerSorter:
	
	static func edge_length_ASC(a, b):
		if a.length < b.length:
			return true
		elif a.index < b.index:
			return true
		return false
	
	static func edge_length_DESC(a, b):
		if a.length > b.length:
			return true
		elif a.index < b.index:
			return true
		return false
	
	static func clone_rotation(a, b):
		if abs(a.rotation) < abs(b.rotation):
			return true
		return false
	
	static func clone_alignment_ASC(a, b):
		if a.alignment < b.alignment:
			return true
		return clone_rotation(a, b)
	
	static func clone_alignment_DESC(a, b):
		if a.alignment > b.alignment:
			return true
		return clone_rotation(a, b)

# keep rotation between -PI and PI
func cap_radian( r:float ) -> float:
	while r > PI:
		r -= TAU
	while r < -PI:
		r += TAU
	return r

# update min & max of a shape
func grow( shape:Dictionary, pos:Vector2 ) -> void:
	if shape.min == null or shape.max == null:
		shape.min = Vector2(pos)
		shape.max = Vector2(pos)
		return
	for i in range(0,2):
		if shape.min[i] > pos[i]:
			shape.min[i] = pos[i]
		if shape.max[i] < pos[i]:
			shape.max[i] = pos[i]
	
# duplicate a vertex structure
func clone_vertex( v:Dictionary ) -> Dictionary:
	return {
		'pos': v.pos,
		'normal': v.normal
	}

# rotation of a vertex, with its normal
func rotated_vertex( v:Dictionary, rot:float ) -> Dictionary:
	return {
		'pos': v.pos.rotated( rot ),
		'normal': v.normal.rotated( rot )
	}

# returns an array of vertices projected on the other side of the vector dir
func mirror_vertices( source:Dictionary, dir:Vector2 ) -> Array:
	var normal:Vector2 =  Vector2( dir.y, -dir.x )
	var cloned_vertices:Array = []
	for v in source.vertices:
		var proj_v:Dictionary = clone_vertex( v )
		proj_v.pos = ( dir * dir.dot(v.pos) - normal * normal.dot(v.pos) )
		cloned_vertices.append( proj_v )
	var output:Array = []
	var vcount:int = source.vertices.size()
	for i in range( 0, vcount ):
		output.append( cloned_vertices[vcount-(i+1)] )
	return output

# compare 2 list of list of vertices
func compare_paths( paths0:Array, paths01:Array ) -> bool:
	var pcount:int = paths0.size()
	if pcount == 0 or pcount != paths01.size():
		return false
	for pi in range(0,pcount):
		if not compare_vertices(paths0[pi], paths01[pi]):
			return false
	return true
	
# compares 2 list of vertices and returns true if all vertices match
func compare_vertices( vertices0:Array, vertices1:Array ) -> bool:
	var vcount:int = vertices0.size()
	if vcount == 0 or vcount != vertices1.size():
		return false
	var v1offset:int = -1
	for i in range(0,vcount):
		if vertices1[i].pos.distance_squared_to( vertices0[0].pos ) <= 1e-3:
			v1offset = i
			break
	if v1offset == -1:
		return false
	for i in range(0,vcount):
		var v0 = vertices0[i].pos
		var v1 = vertices1[(i+v1offset)%vcount].pos
		if v0.distance_squared_to( v1 ) > 1e-3:
			return false
	return true

# connect edges between them (next & previous)
func connect_edges( edges:Array ) -> void:
	for edge in edges:
		if edge.next != null and edge.previous != null:
			continue
		for foreign in edges:
			if foreign.pid != edge.pid:
				# edges are not in the same path!
				continue
			if foreign.v0 == edge.v1:
				edge.next = foreign
				foreign.previous = edge
			if foreign.v1 == edge.v0:
				edge.previous = foreign
				foreign.next = edge
			if edge.next != null and edge.previous != null:
				break

# search for symmetry axis on each vertex and each mid-edge
func compute_symmetry_axis( shape:Dictionary ) -> void:
	
	shape.symmetry_axis = []
# TODO HERE!!!!!!
	for v in shape.vertices:
		var dir:Vector2 = v.pos.normalized()
		var mirrored:Array = mirror_vertices( shape, dir )
		if compare_vertices( mirrored, shape.vertices ):
			shape.symmetry_axis.append( dir )
	
	for e in shape.edges:
		var dir:Vector2 = e.mid.normalized()
		var mirrored:Array = mirror_vertices( shape, dir )
		if compare_vertices( mirrored, shape.vertices ):
			shape.symmetry_axis.append( dir )

# create an inverted copy of the shape
func generate_symmetric( shape:Dictionary, scale:Vector2 ) -> void:
	
	shape.symmetric = new_shape()
	var flipped_vertices:Array = []
	
	for v in shape.vertices:
		var fv:Dictionary = clone_vertex( v )
		fv.pos *= scale
		fv.normal *= scale
		flipped_vertices.append( fv )
		grow( shape, fv.pos )
	
	var vcount:int = flipped_vertices.size()
	for i in range( 0, vcount ):
		var vid:int = ( vcount - i ) % vcount
		shape.symmetric.vertices.append( flipped_vertices[ vid ] )
		
	if compare_vertices( shape.vertices, shape.symmetric.vertices ):
		shape.symmetric = null
		return
	
	for i in range( 0, vcount ):
		var vid:int = ( vcount - i ) % vcount
		var nid = (vid+(vcount-1)) % vcount
		var e:Dictionary = new_edge( i, shape.symmetric.vertices[vid], shape.symmetric.vertices[nid] )
		# flipping normal!
		e.normal *= -1
		shape.symmetric.edges.append( e )
	
	connect_edges( shape.symmetric.edges )
	shape.symmetric.valid = true

# generates a new list of transformed vertices and store it only if it does not exists yet
func clone_shape( shape:Dictionary, list:Array, rot:float, offset:Vector2, symmetric:bool ) -> Dictionary:
	
	var paths:Array = shape.paths
	if symmetric:
		paths = shape.symmetric.paths
	
	var clone:Dictionary = new_clone()
	clone.rotation = cap_radian( rot )
	clone.offset = offset
	clone.symmetry = symmetric
	
	for path in paths:
		var cpath:Array = []
		for v in path:
			var cv:Dictionary = rotated_vertex( v, clone.rotation )
			cv.pos += offset
			cpath.append( cv )
		clone.paths.append(cpath)
	
	var zombies:Array = []
	for other in list:
		if compare_paths( other.paths, clone.paths ) == true:
			if abs(other.rotation) > abs(clone.rotation):
				zombies.append( other )
			else:
				# clone already stored is better than new one >> no need to do anything else
				return clone
	
	for z in zombies:
		list.erase( z )
	
	for path in clone.paths:
		var collider:Array = []
		for cv in clone.vertices:
			collider.append( ( cv.pos - offset ) * shape_collider_scale - cv.normal * shape_main_contraction )
			grow( shape, cv.pos )
		clone.colliders.append( collider )
	
	clone.uid = shape.cloneUID
	clone.valid = true
	shape.cloneUID += 1
	return clone

# copies a clone and apply rotation on vertices
func clone_clone( shape:Dictionary, clone:Dictionary, rot:float, offset:Vector2 ) -> Dictionary:
	var new_clone:Dictionary = new_clone()
	new_clone.uid = shape.cloneUID
	shape.cloneUID += 1
	new_clone.rotation = clone.rotation + rot
	new_clone.offset = offset + clone.offset.rotated(rot)
	new_clone.valid = clone.valid
	for v in clone.vertices:
		var new_v:Dictionary = rotated_vertex( v, rot )
		new_v.pos += offset
		new_clone.vertices.append( new_v )
		new_clone.collider.append( ( new_v.pos - offset ) * shape_collider_scale - new_v.normal * shape_main_contraction )
	return new_clone

# computes the dot product average of previous and next edges of edges after rotation of edge1
func compute_alignment( edge0:Dictionary, edge1:Dictionary, rot:float ) -> float:
	var foreign_prev:Vector2 = edge1.previous.dir.rotated( rot )
	var foreign_next:Vector2 = edge1.next.dir.rotated( rot )
	var foreign_dir:Vector2 = edge1.dir.rotated( rot )
	if edge0.dir.dot( foreign_dir ) < 0:
		# opposite direction, shape have been rotated
		return edge0.previous.dir.dot( foreign_next ) + edge0.next.dir.dot( foreign_prev )
	else:
		# same direction, shape have been mirrored
		return edge0.previous.dir.dot( foreign_prev ) * -1 + edge0.next.dir.dot( foreign_next ) * -1

# create a shape out of a list of Vector2 (no need to repeat last)
func generate_shape( paths:Array ) -> Dictionary:
	
	var shape:Dictionary = new_shape()
	var list2sort:Array
	
	# first: optimise pathes!
	for path in paths:
		var vertices:Array = []
		var collider:Array = []
		for i in range( 0, path.size() ):
			var h = (i+path.size()-1) % path.size()
			var j = (i+1) % path.size()
			var prev_dir:Vector2 = (path[i]-path[h]).normalized()
			var curr_dir:Vector2 = (path[j]-path[i]).normalized()
			if prev_dir.dot(curr_dir) >= 1 - 1e-5:
				continue
			var prev_norm:Vector2 = Vector2( prev_dir.y, -prev_dir.x )
			var next_norm:Vector2 = Vector2( curr_dir.y, -curr_dir.x )
			var norm:Vector2 = (prev_norm+next_norm).normalized()
			vertices.append( new_vertex( path[i], norm ) )
			collider.append( path[i] * shape_collider_scale - norm * shape_main_contraction  )
#			shape.vertices.append( new_vertex( path[i], norm ) )
#			shape.collider.append( path[i] * shape_collider_scale - norm * shape_main_contraction  )
			grow( shape, path[i] )
		if vertices.size() < 3:
			# too few positions in this path!
			continue
		shape.paths.append( vertices )
		shape.colliders.append( collider )
	
	list2sort = []
	for pi in range(0,shape.paths):
		var path:Array = shape.paths[pi]
		for i in range(0,path.size()):
			var j = (i+1) % path.size()
			var edge:Dictionary = new_edge( i, pi, path[i], path[j] )
			shape.edges.append( edge )
			list2sort.append( { "index": i, "length": edge.length } )
	
	list2sort.sort_custom(TilerSorter, "edge_length_ASC")
	for info in list2sort:
		shape.sorting.edge_small.append( info.index )
	list2sort.sort_custom(TilerSorter, "edge_length_DESC")
	for info in list2sort:
		shape.sorting.edge_big.append( info.index )
	
	connect_edges( shape.edges )
	compute_symmetry_axis( shape )
	if shape.symmetry_axis.empty():
		generate_symmetric( shape, Vector2(1,-1) )
	
	for e in shape.edges:
		
		var edge_list:Array = []
		shape.clones.append(edge_list)
		
		for ee in shape.edges:
			if abs(e.length - ee.length) < 1e-5:
				var rot:float = PI + ee.normal.angle_to( e.normal )
				var clone:Dictionary = clone_shape( shape, edge_list, rot, e.mid - ee.mid.rotated( rot ), false )
				if clone.valid:
					edge_list.append( clone )
					# computing alignment
					clone.alignment = compute_alignment( e, ee, rot )
				
		if shape.symmetric != null:
			for ee in shape.symmetric.edges:
				if abs(e.length - ee.length) < 1e-5:
					var rot:float = PI + ee.normal.angle_to( e.normal )
					var clone:Dictionary = clone_shape( shape, edge_list, rot, e.mid - ee.mid.rotated( rot ), true )
					if clone.valid:
						edge_list.append( clone )
						# computing alignment
						clone.alignment = compute_alignment( e, ee, rot )
		
		# sorting clones
		if edge_list.size() > 1:
			
			list2sort = []
			for i in range(0,edge_list.size()):
				var clone:Dictionary = edge_list[i]
				list2sort.append( { "index": i, "alignment": clone.alignment, "rotation": clone.rotation } )
			
			var tmp:Array 
			list2sort.sort_custom(TilerSorter, "clone_rotation")
			tmp = []
			for info in list2sort:
				tmp.append( info.index )
			shape.sorting.clone_rotation.append( tmp )
			
			list2sort.sort_custom(TilerSorter, "clone_alignment_DESC")
			tmp = []
			for info in list2sort:
				tmp.append( info.index )
			shape.sorting.clone_alignment.append( tmp )
			
			list2sort.sort_custom(TilerSorter, "clone_alignment_ASC")
			tmp = []
			for info in list2sort:
				tmp.append( info.index )
			shape.sorting.clone_perpendicular.append( tmp )
		
		else:
			shape.sorting.clone_rotation.append( [0] )
			shape.sorting.clone_alignment.append( [0] )
			shape.sorting.clone_perpendicular.append( [0] )
	
	shape.size = shape.max - shape.min
	shape.valid = shape.edges.size() > 2
	return shape

# generates a quad based having 2 times the given size  
func get_bg_polygon( _origin:Vector2, _size:Vector2 ) -> Array:
	var out:Array = []
	out.append( _origin * 2 * shape_collider_scale )
	out.append( _origin * 2 * shape_collider_scale + _size * Vector2(2,0) * shape_collider_scale )
	out.append( _origin * 2 * shape_collider_scale + _size * Vector2(2,2) * shape_collider_scale )
	out.append( _origin * 2 * shape_collider_scale + _size * Vector2(0,2) * shape_collider_scale )
	return out

### INTERFACE

func create_shape( requestor:Node, positions:Array ) -> void:
	store_shape_creation_request( requestor, positions )
	if creation_thread == null:
		creation_thread = Thread.new()
		creation_thread.start( self, "_thread_creation" )

func create_tiling( requestor:Node, shape:Dictionary, depth:int = 1, edge_sort:int = zellij.EDGE_SORT.DEFAULT, clone_sort:int = zellij.CLONE_SORT.DEFAULT ) -> void:
	store_tiling_request( requestor, new_tiling( shape, depth, edge_sort, clone_sort ) )

### PROCESS

func purge() -> void:
	while get_child_count() > 0:
		remove_child(get_child(0))

func store_shape_creation_request( requestor:Node, positions:Array ) -> void:
	creation_mutex.lock()
	cance_tiling_request( requestor )
	shape_creation_requests.append({
		'requestor': requestor,
		'positions': positions.duplicate(true),
		'timestamp': OS.get_ticks_msec()
	})
	creation_mutex.unlock()

func store_shape_validation_request( requestor:Node, shape:Dictionary ) -> void:
	validation_mutex.lock()
	shape_validation_requests.append({
		'requestor': requestor,
		'shape': shape
	})
	validation_mutex.unlock()

func cance_tiling_request( requestor:Node ) -> void:
	var zombies:Array = []
	for tr in tiling_requests:
		if tr.requestor == requestor:
			tr.tiling.valid = false
			emit_signal( "tiling_finished", tr.requestor, tr.tiling )
			zombies.append( tr )
	for z in zombies:
		tiling_requests.erase( z )
	if !current_tiling_request.empty() and current_tiling_request.requestor == requestor:
		current_tiling_request = {}

func store_tiling_request( requestor:Node, tiling:Dictionary ) -> void:
	# only one tiling request per requestor
	cance_tiling_request( requestor )
	tiling_requests.append({
		'requestor': requestor,
		'tiling': tiling,
		'timestamp': OS.get_ticks_msec()
	})

func _thread_creation(userdata) -> void:
	while !shape_creation_requests.empty():
		creation_mutex.lock()
		var req:Dictionary = shape_creation_requests.pop_front()
		creation_mutex.unlock()
		# pushing shape to validation process
		store_shape_validation_request( req.requestor, generate_shape( req.positions ) )
		emit_signal( "shape_created", req.requestor )
	creation_thread.wait_to_finish()
	creation_thread = null

func new_shape_validation_area2d( position:Vector2 = Vector2.ZERO ) -> Area2D:
	var a:Area2D = shape_area2d.duplicate()
	a.connect( "smart_area_entered", self, "shape_area_entered" )
	add_child( a )
	a.position = position
	return a

func shape_area_entered( local:Area2D, foreign:Area2D ) -> void:
	if current_shape_validation.empty():
		return
	if foreign.data.clone > -1:
		if local.data.bg:
			current_shape_validation.clone_seen += 1
		elif local.data.main:
			current_shape_validation.collision_main.append( foreign.data.clone )

func new_tile_validation_area2d( position:Vector2 = Vector2.ZERO ) -> Area2D:
	var a:Area2D = shape_area2d.duplicate()
	a.connect( "smart_area_entered", self, "tile_area_entered" )
	add_child( a )
	a.position = position
	return a

func tile_area_entered( local:Area2D, foreign:Area2D ) -> void:
	if current_tiling_request.empty():
		return
	if foreign.data.clone != null:
		if !foreign.data.validated:
			if local.data.bg:
				# stop waiting for the clone
				current_tiling_request.area_clone_wait = false
			else:
#				print(foreign.data.clone.uid, "go away!")
				current_tiling_request.area_clone_keep = false

func next_shape_validation() -> void:
	
	validation_mutex.lock()
	var req:Dictionary = shape_validation_requests.pop_front()
	validation_mutex.unlock()
	
	if !req.shape.valid or req.shape.clones.empty():
		print( "skipping validation > shape is corrupted" )
		emit_signal( "shape_validated", req.requestor, req.shape )
		next_shape_validation()
		return
	
	purge()
	
	# recyling request
	req.center = get_viewport().size * 0.5
	req.clone_seen = 0
	req.clone_total = 0
	req.collision_main = []
	req.area_bg = new_shape_validation_area2d( req.center )
	req.area_main = new_shape_validation_area2d( req.center )
	req.area_clones = []
	
	for edge_clones in req.shape.clones:
		for clone in edge_clones:
			req.clone_total += 1
	
	req.area_bg.load_polygon( get_bg_polygon( req.shape.min, req.shape.size ) )
	req.area_bg.data = { "bg": true, "main" : true, "clone": -1 }
	req.area_bg.line(display_collider_line)
	req.area_bg.surface(false)
	
	req.area_main.load_polygon( req.shape.collider )
	req.area_main.data = { "bg": false, "main" : true, "clone": -1 }
	req.area_main.line(display_collider_line)
	req.area_main.surface(false)
	
	current_shape_validation = req

func process_shape_validation() -> void:
	
	# there's a tiling currently processing
	if !current_tiling_request.empty():
		return
	
	if !current_shape_validation.empty():
		
		if current_shape_validation.area_clones.size() != current_shape_validation.clone_total:
			var req:Dictionary = current_shape_validation
			for edge_clones in req.shape.clones:
				for clone in edge_clones:
					var clone_area:Area2D = new_shape_validation_area2d( req.center + clone.offset * shape_collider_scale )
					clone_area.color( Color(0,1,1,0.3) )
					clone_area.load_polygon( clone.collider )
					clone_area.data = { "bg": false, "main" : false, "clone": clone.uid }
					clone_area.line(display_collider_line)
					clone_area.surface(false)
					req.area_clones.append( clone_area )
		
		if current_shape_validation.clone_seen == current_shape_validation.clone_total:
			var req:Dictionary = current_shape_validation
			if !req.collision_main.empty():
				var new_clones:Array = []
				for edge_clones in req.shape.clones:
					var validated_clones:Array = []
					for clone in edge_clones:
						if clone.uid in req.collision_main:
							clone.valid = false
						else:
							clone.valid = true
			
			emit_signal( "shape_validated", req.requestor, req.shape )
			current_shape_validation = {}
	
	elif !shape_validation_requests.empty():
		next_shape_validation()

func next_tiling() -> void:
	
	var req:Dictionary = tiling_requests.pop_front()
	var shape:Dictionary = req.tiling.shape
	
	purge()
	
	# recyling request
	req.center = get_viewport().size * 0.5
	req.area_bg = new_tile_validation_area2d( req.center )
	req.area_main = new_tile_validation_area2d( req.center )
	
	req.area_bg.data = { "bg": true, "main" : false, "clone": null }
	req.area_bg.line(display_collider_line)
	
	req.area_main.load_polygon( shape.collider )
	req.area_main.data = { "bg": false, "main" : true, "clone": null }
	req.area_main.line(display_collider_line)
	
	req.clone_tested = 0
	
	current_tiling_request = req
	
	prepare_tiling_level()

func prepare_tiling_level() -> void:
	
	if !current_shape_validation.empty():
		return
	
	var req:Dictionary = current_tiling_request
	var shape:Dictionary = req.tiling.shape
	
	req.area_bg.load_polygon( get_bg_polygon( req.tiling.min, req.tiling.size ) )
	req.area_bg.line(display_collider_line)
	
	req.area_validated = []
	req.area_clone = null
	req.area_clone_wait = false
	req.area_clone_keep = false
	
	# registration of clones order, once and for all
	req.edge_index = 0
	req.edge_count = 0
	req.clone_index = 0
	# sequence of clones to test, depending on sorting
	req.clones = []
	
	var origins:Array = []
	if req.tiling.levels.empty():
		origins.append({
			"symmetry" : false,
			"offset" : Vector2.ZERO,
			"rotation" : 0
		})
	else:
		var curr_level:Array = req.tiling.levels.back()
		for clone in curr_level:
			origins.append({
				"symmetry" : clone.symmetry,
				"offset" : clone.offset,
				"rotation" : clone.rotation
			})
	
	
	var edge_indices:Array
	var clone_indices:Array
	match req.tiling.edge_sort:
		zellij.EDGE_SORT.DEFAULT:
			for i in range(0,shape.edges.size()):
				edge_indices.append(i)
		zellij.EDGE_SORT.INVERSE:
			var ecount:int = shape.edges.size()
			for i in range(0,ecount):
				edge_indices.append(ecount-(i+1))
		zellij.EDGE_SORT.SMALL:
			edge_indices = shape.sorting.edge_small
		zellij.EDGE_SORT.BIG:
			edge_indices = shape.sorting.edge_big
	
	for origin in origins:
		for eid in edge_indices:
			var clones:Array = shape.clones[eid]
			match req.tiling.clone_sort:
				zellij.CLONE_SORT.DEFAULT:
					clone_indices = shape.sorting.clone_rotation[eid]
				zellij.CLONE_SORT.ALIGN:
					clone_indices = shape.sorting.clone_alignment[eid]
				zellij.CLONE_SORT.PERPENDICULAR:
					clone_indices = shape.sorting.clone_perpendicular[eid]
			var clist:Array = []
			for cid in clone_indices:
				if clones[cid].valid:
					clist.append( { "clone": clones[cid], "origin": origin } )
			req.clones.append( clist )
			req.edge_count += 1
	
	tiling_stats()

func tiling_stats() -> void:
	
	if !current_shape_validation.empty():
		return
	
	var req:Dictionary = current_tiling_request
	req.tiling.stats.edge_index = req.edge_index
	req.tiling.stats.edge_count = req.edge_count
	req.tiling.stats.clone_index = req.clone_index
	req.tiling.stats.clone_validated = req.area_validated.size()
	req.tiling.stats.clone_tested = req.clone_tested
	req.tiling.stats.tiles = 0
	for lvl in req.tiling.levels:
		req.tiling.stats.tiles += lvl.size()

func process_tiling() -> void:
	
	# there's a shapes currently processing
	if !current_shape_validation.empty():
		return
	
	if !current_tiling_request.empty():
		
		var req:Dictionary = current_tiling_request
		var shape:Dictionary = req.tiling.shape
		
		if req.tiling.levels.size() == req.tiling.depth_request:
			req.tiling.valid = true
			emit_signal( "tiling_finished", req.requestor, req.tiling )
			purge()
			current_tiling_request = {}
			return
		
		# a new collision request have been posted
		if req.area_clone_wait:
			return
		
		# collision processing (see tile_area_entered) is finished, time to prepare next one...
		elif req.area_clone != null:
			if req.area_clone_keep:
				req.area_clone.color( Color(1,1,1,.6) )
				req.area_clone.line(display_collider_line)
				req.area_clone.data.validated = true
				req.area_validated.append( req.area_clone )
				# we have a good solution, we can go directly to next edge!
				req.edge_index += 1
				req.clone_index = 0
			else:
				remove_child( req.area_clone )
				req.area_clone.queue_free()
			req.area_clone = null
		
		# going through all edges and clones of current level
		if req.edge_index < req.edge_count:
			# moving to next edge
			while req.edge_index < req.edge_count and req.clone_index >= req.clones[req.edge_index].size():
				req.edge_index += 1
				req.clone_index = 0
			if req.edge_index < req.edge_count:
				var cdata:Dictionary = req.clones[req.edge_index][req.clone_index]
				var origin:Dictionary = cdata.origin
				var clone:Dictionary = cdata.clone
				req.area_clone = new_tile_validation_area2d( req.center + origin.offset )
				req.area_clone.load_polygon( clone.collider )
				req.area_clone.position = req.center + ( origin.offset + clone.offset.rotated( origin.rotation ) ) * shape_collider_scale
				req.area_clone.rotation = origin.rotation
				req.area_clone.line(display_collider_line)
				req.area_clone.surface(false)
				req.area_clone.data = { 
					"bg": false, 
					"main" : false, 
					"origin":  origin,
					"clone":  clone,
					"validated": false
				}
				req.area_clone_wait = true
				req.area_clone_keep = true
				req.clone_index += 1
				req.clone_tested += 1
		
		else:
			# reached the end of this level => let's backup all the clones and prepare next level
			var new_level:Array = []
			for area in req.area_validated:
				area.data.clone = clone_clone( req.tiling.shape, area.data.clone, area.data.origin.rotation, area.data.origin.offset )
				area.color( Color(1,1,1,.3) )
				if display_collider_surface:
					area.line(false)
					area.surface(display_collider_surface)
				else:
					area.line(display_collider_line)
				new_level.append( area.data.clone )
				# grow tiling!
				for v in area.data.clone.vertices:
					grow( req.tiling, v.pos )
			req.tiling.size = req.tiling.max - req.tiling.min
			req.tiling.levels.append( new_level )
			emit_signal( "tiling_step", req.requestor, req.tiling )
			prepare_tiling_level()
		
		tiling_stats()
	
	elif !tiling_requests.empty():
		next_tiling()

### ENGINE LINKS

func _process(delta) -> void:
	process_shape_validation()
	process_tiling()

func _exit_tree() -> void:
	if creation_thread != null:
		creation_thread.wait_to_finish()

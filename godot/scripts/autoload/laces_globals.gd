extends Node

# version
const LACE_VERSION = 0.4

# paths
const LACE_DATAPATH = "user://all_laces.json"

# defaults
const LACE_DEFAULT_SEGMENT_MAX = 20
const LACE_DEFAULT_SWITCH_MIN = 1
const LACE_DEFAULT_SWITCH_MAX = 3
const LACE_DEFAULT_LENGTH = 60
const LACE_DEFAULT_GAP = 15
const LACE_DEFAULT_WIDTH = 4

#enums

# slider value types
const NUMBLOCK_TYPE_INT = 0
const NUMBLOCK_TYPE_FLOAT = 1

# rulers display
const LACE_RULER_NONE = 0
const LACE_RULER_SIMPLE = 1
const LACE_RULER_FULL = 2
const LACE_RULER_MAX = 3

# laces point data type
const LACE_PTYPE_DIR = 0
const LACE_PTYPE_SWITCH = 1
const LACE_PTYPE_START = 2
const LACE_PTYPE_LENGTH = 3
const LACE_PTYPE_GAP = 4
const LACE_PTYPE_LENGTHGAP = 5

# laces direction
const LACE_DIR_LENGTH = int(2)
const LACE_DIR_GAP = int(1)

# data structure
func line_configuration( 
	init_length = LACE_DEFAULT_LENGTH, 
	init_gap = LACE_DEFAULT_GAP, 
	width = LACE_DEFAULT_WIDTH ):
	return {
		# generation
		"segment_max": LACE_DEFAULT_SEGMENT_MAX,
		"switch_min": LACE_DEFAULT_SWITCH_MIN,
		"switch_max": LACE_DEFAULT_SWITCH_MAX,
		# initial direction
		"dir": [LACE_DIR_LENGTH,LACE_DIR_GAP],
		# start length
		"length": init_length,
		# size of gap
		"gap": init_gap,
		# list of switch count 
		"switches": [],
		# segment count
		"segments": 0,
		# stroke width
		"width": width,
		"joint": Line2D.LINE_JOINT_SHARP,
		"cap": Line2D.LINE_CAP_BOX,
		# limits
		"width_max": -1,
		"height_max": -1
	}

func check_configuration( conf ):
	if not conf is Dictionary:
		conf = line_configuration()
		return
	var tmpl = line_configuration()
	var keys = tmpl.keys()
	# adding missing keys
	for k in keys:
		if not k in conf:
			conf[k] = tmpl[k]
	# removing wrong keys
	var cks = conf.keys()
	for k in cks:
		if not k in tmpl:
			conf.erase( k )

func compare_configurations( conf0, conf1 ):
	if not conf0 is Dictionary or not conf1 is Dictionary:
		return false
	var keys = conf0.keys()
	for k in keys:
		if not k in conf1:
			return false
		if conf0[k] is Array:
			var l = len( conf0[k] )
			if l != len( conf1[k] ):
				return false
			for i in range( 0, l ):
				if conf0[k][i] != conf1[k][i]:
					return false
		elif conf0[k] is Dictionary:
			if not compare_configurations( conf0[k], conf1[k] ):
				return false
		else:
			if conf0[k] != conf1[k]:
				return false
	return true
			
	
# 300dpi to mm
const to_dpi = 1.0/300 * 25.4

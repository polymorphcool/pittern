extends PanelContainer

signal file_selected
signal cancel

const dir_default:String = 'user://'
export(String) var dir_path:String = ''
export(String) var extension_filter:String = ''
export(String) var directory_prefix:String = '-[ '
export(String) var file_prefix:String = ''
export(bool) var display_hidden:bool = false
export(int) var columns:int = 1
export(int) var max_characters:int = 30
export(int) var max_item_per_page:int = 10
export(bool) var enabled:bool = false setget set_enable

var extensions:Array = []
var current_path:String = ''
var directories:Array = []
var files:Array = []
var current_page:int = 0
var total_pages:int = 0
var historic:Dictionary = {}

var file_selected:String = ''

func purge() -> void:
	while $general/list.get_child_count() > 0:
		$general/list.remove_child($general/list.get_child(0))

func get_absolute_dir( path:String ) -> String:
	var d:Directory = Directory.new()
	var tmp:File = File.new()
	var tmpname:String = str( OS.get_unix_time() ) + "filebrowser"
	tmp.open( path + tmpname, File.WRITE )
	var absolute_path:String = tmp.get_path_absolute()
	tmp.close()
	d.open( current_path )
	d.remove( absolute_path )
	var parts:Array = absolute_path.split("/")
	absolute_path = ''
	for i in range( 0, parts.size()-1 ):
		absolute_path += parts[i] + '/'
	return absolute_path

func get_previous_folder( path:String ) -> String:
	var parts:Array = path.split("/")
	var previous_path:String = ''
	for i in range( 0, max(1, parts.size()-2) ):
		previous_path += parts[i] + '/'
	return previous_path

func load_folder( dp:String ) -> void:
	
	var d:Directory = Directory.new()
	if dp == '' or not d.dir_exists( dp ):
		current_path = get_absolute_dir( dir_default )
	else:
		current_path = dp
	
	if current_path.substr(current_path.length()-1,1) != '/':
		current_path += '/'
	
	# reset arrays
	directories = []
	files = []
	file_selected = ''
	extensions = extension_filter.split(',',false)
	for i in range(0,extensions.size()):
		extensions[i] = extensions[i].replace(' ', '')
	$general/list.columns = columns
	$general/op/open.disabled = true
	
	# loading current directory
	var f:File = File.new()
	if d.open( current_path ) == OK:
		d.list_dir_begin()
		var fname:String = d.get_next()
		while fname != "":
			var fpath = current_path + fname
			if fname == '.' or fname == '..':
				pass
			elif fname.begins_with('.') and !display_hidden:
				pass
			else:
				if d.current_is_dir():
					directories.append( fpath )
				else:
					files.append( fpath )
			fname = d.get_next()
	else:
		purge()
		$general/location.text = "ERROR opening " + current_path
		return
	
	directories.sort()
	files.sort()
	total_pages = ceil( ( directories.size() + files.size() ) * 1.0 / max_item_per_page )
	current_page = -1
	$general/location.text = d.get_current_dir()
	
	if not current_path in historic.keys():
		historic[current_path] = 0
	
	load_page(historic[current_path])

func prev_page() -> void:
	load_page( current_page - 1 )

func next_page() -> void:
	load_page( current_page + 1 )

func get_label( text:String, is_folder:bool ) -> String:
	if max_characters == -1 or text.length() <= max_characters:
		return text
	if is_folder:
		return text.substr( 0, max_characters-3 ) + '...'
	else:
		var extdot:int = text.find_last('.')
		var extlen:int = text.length()-extdot
		return text.substr( 0, (max_characters-extlen)-3 ) + '...' + text.substr( extdot, extlen )

func load_page( page:int ) -> void:
	
	purge()
	
	if page < 0:
		page = 0
	elif total_pages > 1 and page >= total_pages:
		page = total_pages - 1
	
	var start_at:int = page * max_item_per_page
	var stop_at:int = (page+1) * max_item_per_page
	
	if current_page == page:
		return
	
	current_page = page
	historic[current_path] = current_page
	
	if total_pages > 1:
		$general/nav.visible = true
		$general/nav/prev.disabled = current_page == 0
		$general/nav/next.disabled = current_page == (total_pages-1)
		$general/nav/spcr.text = str(current_page+1)+'/'+str(total_pages)
	else:
		$general/nav.visible = false
	
	var dbtn:Button
	# up one folder
	dbtn = $general/dir_btn.duplicate()
	dbtn.data = get_previous_folder( current_path )
	dbtn.disabled = dbtn.data == current_path
	dbtn.text = directory_prefix + '..'
	dbtn.visible = true
	dbtn.connect("smart_button_pressed",self,"dir_pressed")
	$general/list.add_child(dbtn)
	
	for i in range(1,$general/list.columns):
		var spcr:Label = $general/spcr.duplicate()
		spcr.visible = true
		$general/list.add_child(spcr)
	
	var bid:int = 0
	for dir in directories:
		if bid >= start_at:
			dbtn = $general/dir_btn.duplicate()
			dbtn.data = dir
			var parts:Array = dir.split('/', false)
			dbtn.text = get_label( directory_prefix + parts[ parts.size()-1 ], true )
			dbtn.visible = true
			dbtn.connect("smart_button_pressed",self,"dir_pressed")
			dbtn.connect("smart_button_multiple_pressed",self,"dir_mpressed")
			$general/list.add_child(dbtn)
		bid += 1
		if bid >= stop_at:
			break
	for file in files:
		if bid < stop_at and bid >= start_at:
			var fbtn:Button = $general/file_btn.duplicate()
			fbtn.data = file
			var parts:Array = file.split('/', false)
			fbtn.text = get_label( file_prefix + parts[ parts.size()-1 ], false )
			fbtn.visible = true
			fbtn.disabled = !extensions.empty()
			for ext in extensions:
				if file.ends_with(ext):
					fbtn.disabled = false
					break
			fbtn.connect("smart_button_pressed",self,"file_pressed")
			fbtn.connect("smart_button_multiple_pressed",self,"file_mpressed")
			$general/list.add_child(fbtn)
		bid += 1
		if bid >= stop_at:
			break
	
	rect_size = Vector2.ZERO

func dir_pressed( btn:Button ) -> void:
	load_folder( btn.data )

func file_pressed( btn:Button ) -> void:
	file_selected = btn.data
	$general/op/open.disabled = file_selected == ''

func file_mpressed( btn:Button ) -> void:
	open_selected()

func open_selected() -> void:
	emit_signal( "file_selected", file_selected )
	print( "OPEN ", file_selected )

func cancel_selected() -> void:
	emit_signal( "cancel" )
	set_enable( false )

func set_enable(b:bool) -> void:
	enabled = b
	$general/nav.visible = enabled
	$general/op.visible = enabled
	if !enabled:
		purge()
	else:
		load_folder( dir_path )

func _ready():
	$general/nav.visible = false
	$general/op.visible = false
	$general/nav/prev.connect("pressed",self,"prev_page")
	$general/nav/next.connect("pressed",self,"next_page")
	$general/op/open.connect("pressed",self,"open_selected")
	$general/op/cancel.connect("pressed",self,"cancel_selected")
